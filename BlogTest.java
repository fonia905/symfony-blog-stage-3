import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static io.restassured.RestAssured.given;


public class BlogTest {
    private final String LOGINURL = "https://test-stand.gb.ru/gateway/login";
    private final String POSTSURL = "https://test-stand.gb.ru/api/posts";
    private final String USERNAME = "Dima";
    private final String PASSWORD = "4085c6bd95";
    private final String TOKEN = "e75a284d01c18fc3972f35b4cd7fada0";

    RequestSpecification requestSpecification;

    @BeforeEach
    void addToken() {
        requestSpecification = new RequestSpecBuilder()
                .addHeader("X-Auth-Token", TOKEN)
                .build();
    }

    @Test
    void loginTest() {
        given()
                .formParam("username", USERNAME)
                .formParam("password", PASSWORD)
                .when()
                .post(LOGINURL)
                .then()
                .assertThat()
                .statusCode(200);
    }

    @Test
    void loginCyrillicTest() {
        given()
                .formParam("username", "привет")
                .formParam("password", "привет")
                .when()
                .post(LOGINURL)
                .then()
                .assertThat()
                .statusCode(401);
    }

    @Test
    void loginMoreThan20Test() {
        given()
                .formParam("username", "12345678901234567890123")
                .formParam("password", PASSWORD)
                .when()
                .post(LOGINURL)
                .then()
                .assertThat()
                .statusCode(401);
    }

    @Test
    void loginEmptyTest() {
        given()
                .formParam("username", "")
                .formParam("password", "")
                .when()
                .post(LOGINURL)
                .then()
                .assertThat()
                .statusCode(401);
    }

    @Test
    void myPostWithoutLoginTest() {
        given()
                .when()
                .get(POSTSURL)
                .then()
                .assertThat()
                .statusCode(401);
    }

    @Test
    void myPostWithLoginTest() {
        given()
                .spec(requestSpecification)
                .when()
                .get(POSTSURL)
                .then()
                .assertThat()
                .contentType(ContentType.JSON)
                .statusCode(200);
    }

    @Test
    void myPostAsc1Test() {
        JsonPath response = given()
                .spec(requestSpecification)
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "1")
                .when()
                .get(POSTSURL)
                .jsonPath();
        int id = response.get("data[1].id");
        assertThat(response.get("data[0].id"), lessThan(id));
    }

    @Test
    void myPostAsc2Test() {
        JsonPath response = given()
                .spec(requestSpecification)
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "2")
                .when()
                .get(POSTSURL)
                .jsonPath();
        int id = response.get("data[1].id");
        assertThat(response.get("data[0].id"), lessThan(id));
    }

    @Test
    void myPostAsc3Test() {
        given()
                .spec(requestSpecification)
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "3")
                .when()
                .get(POSTSURL)
                .then()
                .assertThat()
                .contentType(ContentType.JSON)
                .statusCode(200);
    }

    @Test
    void myPostDesc1Test() {
        JsonPath response = given()
                .spec(requestSpecification)
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page", "1")
                .when()
                .get(POSTSURL)
                .jsonPath();
        int id = response.get("data[1].id");
        assertThat(response.get("data[0].id"), greaterThan(id));
    }

    @Test
    void myPostDesc2Test() {
        JsonPath response = given()
                .spec(requestSpecification)
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page", "2")
                .when()
                .get(POSTSURL)
                .jsonPath();
        int id = response.get("data[1].id");
        assertThat(response.get("data[0].id"), greaterThan(id));
    }

    @Test
    void notMyPostAsc1Test() {
        JsonPath response = given()
                .spec(requestSpecification)
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "1")
                .when()
                .get(POSTSURL)
                .jsonPath();
        int id = response.get("data[1].id");
        assertThat(response.get("data[0].id"), lessThan(id));
    }

    @Test
    void notMyPostAsc100Test() {
        JsonPath response = given()
                .spec(requestSpecification)
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "100")
                .when()
                .get(POSTSURL)
                .jsonPath();
        int id = response.get("data[1].id");
        assertThat(response.get("data[0].id"), lessThan(id));
    }

    @Test
    void notMyPostAsc10000Test() {
        given()
                .spec(requestSpecification)
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "10000")
                .when()
                .get(POSTSURL)
                .then()
                .assertThat()
                .contentType(ContentType.JSON)
                .statusCode(200);
    }

    @Test
    void notMyPostDesc1Test() {
        given()
                .spec(requestSpecification)
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page", "1")
                .when()
                .get(POSTSURL)
                .then()
                .assertThat()
                .contentType(ContentType.JSON)
                .statusCode(200);
    }

    @Test
    void notMyPostDesc2Test() {
        given()
                .spec(requestSpecification)
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page", "2")
                .when()
                .get(POSTSURL)
                .then()
                .assertThat()
                .contentType(ContentType.JSON)
                .statusCode(200);
    }
}
